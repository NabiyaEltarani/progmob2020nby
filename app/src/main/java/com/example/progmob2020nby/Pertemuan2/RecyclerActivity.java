package com.example.progmob2020nby.Pertemuan2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.progmob2020nby.Adapter.MahasiswaRecyclerAdapter;
import com.example.progmob2020nby.Model.Mahasiswa;
import com.example.progmob2020nby.R;

import java.util.ArrayList;
import java.util.List;

public class RecyclerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler);

        RecyclerView rv = (RecyclerView)findViewById(R.id.rvLatihan);
        MahasiswaRecyclerAdapter mahasiswaRecyclerAdapter;

        //data

        List<Mahasiswa> mahasiswaList = new ArrayList<>();

        //generate data mahasiswa
        Mahasiswa m1 = new Mahasiswa("Argo", "72180201", "082324356892");
        Mahasiswa m2 = new Mahasiswa("Bambang", "72180222", "082324212892");
        Mahasiswa m3 = new Mahasiswa("Basri", "72180290", "081245346892");
        Mahasiswa m4 = new Mahasiswa("Angel", "72180257", "081323458734");
        Mahasiswa m5 = new Mahasiswa("Lisa", "72180249", "085684392017");

        mahasiswaList.add(m1);
        mahasiswaList.add(m2);
        mahasiswaList.add(m3);
        mahasiswaList.add(m4);
        mahasiswaList.add(m5);

        mahasiswaRecyclerAdapter = new MahasiswaRecyclerAdapter ( RecyclerActivity.this);
        mahasiswaRecyclerAdapter.setMahasiswaList(mahasiswaList);

        rv.setLayoutManager(new LinearLayoutManager( RecyclerActivity.this));
        rv.setAdapter(mahasiswaRecyclerAdapter);


    }
}