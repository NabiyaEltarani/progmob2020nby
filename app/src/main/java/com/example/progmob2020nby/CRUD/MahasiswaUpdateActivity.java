package com.example.progmob2020nby.CRUD;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.progmob2020nby.Model4.DefaultResult;
import com.example.progmob2020nby.Network.GetDataService;
import com.example.progmob2020nby.Network.RetrofitClientInstance;
import com.example.progmob2020nby.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MahasiswaUpdateActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mahasiswa_update);

        EditText editTextNimCari = (EditText)findViewById(R.id.editTextNimCari);
        EditText editTextNama2 = (EditText)findViewById(R.id.editTextNama2);
        EditText editTextNim2 = (EditText)findViewById(R.id.editTextNim2);
        EditText editTextAlamat2 = (EditText)findViewById(R.id.editTextAlamat2);
        EditText editTextEmail2 = (EditText)findViewById(R.id.editTextEmail2);
        Button buttonUbah = (Button) findViewById(R.id.buttonUbah);



        pd = new ProgressDialog(MahasiswaUpdateActivity.this);

        buttonUbah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon Tunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.update_mhs(
                        editTextNimCari.getText().toString(),
                        editTextNama2.getText().toString(),
                        editTextNim2.getText().toString(),
                        editTextAlamat2.getText().toString(),
                        editTextEmail2.getText().toString(),
                        "Fotonya Di Ubah",
                        "72180215"
                        );


                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(MahasiswaUpdateActivity.this, "Data Berhasil Diubah", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(MahasiswaUpdateActivity.this, "Gagal", Toast.LENGTH_LONG).show();
                    }
                });


            }
        });


    }
}
