package com.example.progmob2020nby.CRUD;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.example.progmob2020nby.R;


public class MainDosenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_dosen);

        Button btnTambahDosen = (Button)findViewById(R.id.btnTambahDosen);
        Button btnDataDosen = (Button)findViewById(R.id.btnDataDosen);
        Button btnUbahDosen = (Button)findViewById(R.id.btnUbahDosen);
        Button btnHapusDosen = (Button)findViewById(R.id.btnHapusDosen);

        //action
        btnDataDosen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainDosenActivity.this, DaftarDosenActivity.class);
                startActivity(intent);
            }
        });
        btnTambahDosen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainDosenActivity.this,DosenCreateActivity.class);
                startActivity(intent);
            }
        });
        btnUbahDosen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainDosenActivity.this, DosenUpdateActivity.class);
                startActivity(intent);
            }
        });
        btnHapusDosen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainDosenActivity.this, DosenDeleteActivity.class);
                startActivity(intent);
            }
        });

    }
}