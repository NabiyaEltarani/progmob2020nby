package com.example.progmob2020nby.CRUD;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.progmob2020nby.Model4.DefaultResult;
import com.example.progmob2020nby.Network.GetDataService;
import com.example.progmob2020nby.Network.RetrofitClientInstance;
import com.example.progmob2020nby.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DosenUpdateActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dosen_update);

        Button btnUbah;
        ProgressDialog pd;

        EditText edNidnLama = (EditText)findViewById(R.id.edNidnLama);
        EditText edNamaBaru = (EditText)findViewById(R.id.edNamaBaruDosen);
        EditText edNidnBaru = (EditText)findViewById(R.id.edNidnBaruDosen);
        EditText edAlamatBaru = (EditText)findViewById(R.id.edAlamatBaruDosen);
        EditText edEmailBaru = (EditText)findViewById(R.id.edEmailBaruDosen);
        EditText edGelarBaru = (EditText)findViewById(R.id.edGelarBaruDosen);
        Button btnSimpanUbah = (Button)findViewById(R.id.btnSimpanUbahDosen);
        pd = new ProgressDialog(DosenUpdateActivity.this);


        Intent data = getIntent();
        edNidnLama.setText(data.getStringExtra("nim"));
        edNamaBaru.setText(data.getStringExtra("nama"));
        edNidnBaru.setText(data.getStringExtra("nim"));
        edAlamatBaru.setText(data.getStringExtra("alamat"));
        edGelarBaru.setText(data.getStringExtra("email"));

        btnSimpanUbah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Silahkan Tunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.update_mhs(
                        edNamaBaru.getText().toString(),
                        edNidnBaru.getText().toString(),
                        edAlamatBaru.getText().toString(),
                        edEmailBaru.getText().toString(),
                        edGelarBaru.getText().toString(),
                        "Fotonya Diubah",
                        "72180204"
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(DosenUpdateActivity.this, "Data Berhasil Dihapus", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(DosenUpdateActivity.this, "Gagal", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}