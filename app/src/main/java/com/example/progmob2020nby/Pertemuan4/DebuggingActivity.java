package com.example.progmob2020nby.Pertemuan4;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.progmob2020nby.Adapter.MahasiswaRecyclerAdapter;
import com.example.progmob2020nby.Adapter4.DebuggingRecyclerAdapter;
import com.example.progmob2020nby.Model.Mahasiswa;
import com.example.progmob2020nby.Model4.MahasiswaDebugging;
import com.example.progmob2020nby.R;

import java.util.ArrayList;
import java.util.List;

public class DebuggingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_debugging);

        RecyclerView rv = (RecyclerView)findViewById(R.id.rvPertemuan);
        DebuggingRecyclerAdapter debuggingRecyclerAdapter;

        //data dummy
        List<MahasiswaDebugging> mahasiswaList = new ArrayList<MahasiswaDebugging>();

        //generate data mahasiswa
        MahasiswaDebugging m1 = new MahasiswaDebugging("Argo", "72180201", "082324356892");
        MahasiswaDebugging m2 = new MahasiswaDebugging("Bambang", "72180222", "082324212892");
        MahasiswaDebugging m3 = new MahasiswaDebugging("Basri", "72180290", "081245346892");
        MahasiswaDebugging m4 = new MahasiswaDebugging("Angel", "72180257", "081323458734");
        MahasiswaDebugging m5 = new MahasiswaDebugging("Lisa", "72180249", "085684392017");

        mahasiswaList.add(m1);
        mahasiswaList.add(m2);
        mahasiswaList.add(m3);
        mahasiswaList.add(m4);
        mahasiswaList.add(m5);


        debuggingRecyclerAdapter = new DebuggingRecyclerAdapter(DebuggingActivity.this);
        debuggingRecyclerAdapter.setMahasiswaList(mahasiswaList);

        rv.setLayoutManager(new LinearLayoutManager(DebuggingActivity.this));
        rv.setAdapter(debuggingRecyclerAdapter);
    }
}
