package com.example.progmob2020nby.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.progmob2020nby.Model.Matkul;
import com.example.progmob2020nby.R;

import java.util.ArrayList;
import java.util.List;


public class MatkulCRUDRecyclerAdapter extends RecyclerView.Adapter<MatkulCRUDRecyclerAdapter.ViewHolder> {
    private Context context;
    private List<Matkul> matakuliahList;

    public MatkulCRUDRecyclerAdapter(Context context){
        this.context = context;
        matakuliahList = new ArrayList<>();
    }

    public MatkulCRUDRecyclerAdapter(List<Matkul> matakuliahList){
        this.matakuliahList = matakuliahList;
    }

    public List<Matkul> getMatakuliahList(){
        return matakuliahList;
    }

    public void setMatakuliahList(List<Matkul> matakuliahList){
        this.matakuliahList = matakuliahList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_cardview_matkul,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Matkul mk = matakuliahList.get(position);

        holder.tvNama.setText(mk.getNama());
        holder.tvKode.setText(mk.getKode());
        holder.tvHari.setText(Integer.toString(mk.getHari()));
        holder.tvSesi.setText(Integer.toString(mk.getSesi()));
        holder.tvSks.setText(Integer.toString(mk.getSks()));

    }

    @Override
    public int getItemCount() {
        return matakuliahList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView tvNama;
        public TextView tvKode;
        public TextView tvHari;
        public TextView tvSesi;
        public TextView tvSks;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvNama = itemView.findViewById(R.id.tvNama);
            tvKode = itemView.findViewById(R.id.tvKode);
            tvHari = itemView.findViewById(R.id.tvHari);
            tvSesi = itemView.findViewById(R.id.tvSesi);
            tvSks = itemView.findViewById(R.id.tvSks);
        }
    }
}
