package com.example.progmob2020nby.Pertemuan6;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.progmob2020nby.R;


public class PrefActivity extends AppCompatActivity {
    String isLogin = "";

            @Override
            protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                setContentView(R.layout.activity_pref);

                Button btnPref = (Button)findViewById(R.id.btnPref);
                final EditText username = findViewById(R.id.txtUsername);
                EditText password = findViewById(R.id.txtPassword);

                SharedPreferences pref = PrefActivity.this.getSharedPreferences("prefs_file",MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();


                isLogin = pref.getString("isLogin","0");
                if (isLogin.equals("1")){
                    btnPref.setText("Logout");
                }else{
                    btnPref.setText("Login");
                }

                btnPref.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        isLogin = pref.getString("isLogin",null);
                        if ((username.getText().toString().contains("72180215"))&&(password.getText().toString().contains("progmob"))){
                            editor.putString("isLogin","Mahasiswa");
                            editor.commit();
                            Toast.makeText(PrefActivity.this,"Horeee berhasil", Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(PrefActivity.this, HomeActivity.class);
                            startActivity(intent);
                        }else{
                            Toast.makeText(PrefActivity.this,"Nim dan Nama salah !", Toast.LENGTH_LONG).show();
                            btnPref.setEnabled(false);
                        }
                    }
                });
            }
        }